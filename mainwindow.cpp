#include "mainwindow.h"
#include "ui_mainwindow.h"

#include <QDebug>

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    m_ui(new Ui::MainWindow())
{
    m_ui->setupUi(this);
}

MainWindow::~MainWindow() = default;

void MainWindow::LogLabelGeometry()
{
    qDebug() << "textLabel->geometry()     :" << m_ui->textLabel->geometry();
    qDebug() << "textLabel->sizeHint()     :" << m_ui->textLabel->sizeHint();
    qDebug() << "textLabel->minimumSizeHint:" << m_ui->textLabel->minimumSizeHint();
}

void MainWindow::SetLabelText(const QString &text)
{
    this->m_ui->textLabel->setText(text);
}
