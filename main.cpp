#include "mainwindow.h"

#include <QApplication>
#include <QTimer>

int main(int argc, char *argv[])
{
    QApplication app(argc, argv);
    MainWindow w;
    w.show();

    /* 
     * Compile the project and launch the app. If the following two
     * lines are commented out, then the labels width will be equal
     * to 8, even though there is no text.
     *
     * If the text is changed from a none empty string to an empty
     * one, the labels width will be equal to 0.
     */

    // w.SetLabelText("x");
    // w.SetLabelText("");
    QTimer::singleShot(500, &w, &MainWindow::LogLabelGeometry);

    return app.exec();
}
