# QLabelLayoutErrorExample

Small example project to show a QLabel layouting problem with no text

## The debug output with both lines as comments

```
textLabel->geometry()     : QRect(0,0 8x300)
textLabel->sizeHint()     : QSize(8, 18)
textLabel->minimumSizeHint: QSize(8, 18)
```

## The debug output with both lines as code

```
textLabel->geometry()     : QRect(0,0 0x300)
textLabel->sizeHint()     : QSize(0, 18)
textLabel->minimumSizeHint: QSize(0, 18)
```
